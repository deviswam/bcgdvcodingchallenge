//
//  UserAccountAPIClient.swift
//  BCGDigitalVenturesTest
//
//  Created by Waheed Malik on 10/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

enum UserAccountAPIClientError: Error {
    case httpError
    case invalidDataError
    case dataSerializationError
}

enum UserAccountAPIClientMethod : String {
    case createSession = "/sessions/new"
    case getUserDetails = "/users/:userid"
    case getAndPostAvatar = "/users/:userid/avatar"
}

enum HttpMethod: String {
    case get = "GET"
    case post = "POST"
}

protocol UserAccountAPIClient {
    func createSession(with email: String, password: String, completionHandler: @escaping (_ data: (userId: Int, token: String)?, _ error: UserAccountAPIClientError?) -> Void)
    func getDetails(of userId: Int, token: String, completionHandler: @escaping (_ data: (email: String, password: String, avatarUrl: String)?, _ error: UserAccountAPIClientError?) -> Void)
    func createAvatar(of userId: Int, with image: Data, token: String, completionHandler: @escaping (_ avatarUrl: String?, _ error: UserAccountAPIClientError?) -> Void)
    func getUserAvatar(from avatarUrl: String, token: String, completionHandler: @escaping (_ avatar: Data?, _ error: UserAccountAPIClientError?) -> Void)
}

class BCGUserAccountAPIClient: UserAccountAPIClient {
    // MARK: PRIVATE VARIABLES
    private let urlSession: URLSession!
    
    // MARK: INITIALIZER
    init(urlSession: URLSession = URLSession.shared) {
        self.urlSession = urlSession
    }
    
    func createSession(with email: String, password: String, completionHandler: @escaping (_ data: (userId: Int, token: String)?, _ error: UserAccountAPIClientError?) -> Void) {
        
        let headerParams = ["Content-Type" : "application/json"]
        let bodyParams = ["email": email, "password": password]
        let method = UserAccountAPIClientMethod.createSession.rawValue
        guard let request = self.createURLRequest(from: method, httpMethod: .post, headerParams: headerParams, bodyParams: bodyParams) else {
            return
        }
        self.execute(request: request) { (data, error) in
            //print("result:", String(bytes: data!, encoding: .utf8)!)
            var retData: (Int, String)?
            var retError: UserAccountAPIClientError?
            
            if error != nil {
                retError = .httpError
            } else if let data = data {
                do {
                    if let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] {
                        guard let userId = dictionary["userid"] as? String, let iUserId = Int(userId),
                              let token = dictionary["token"] as? String else {
                            completionHandler(nil, .invalidDataError)
                            return
                        }
                        retData = (iUserId, token)
                    }
                } catch {
                    retError = .dataSerializationError
                }
            }
            completionHandler(retData, retError)
        }
    }
    
    func getDetails(of userId: Int, token: String, completionHandler: @escaping (_ data: (email: String, password: String, avatarUrl: String)?, _ error: UserAccountAPIClientError?) -> Void) {
        let headerParams = ["Authorization" : "Token \(token)"]
        let method = UserAccountAPIClientMethod.getUserDetails.rawValue.replacingOccurrences(of: ":userid", with: String(userId))
        guard let request = self.createURLRequest(from: method, httpMethod: .get, headerParams: headerParams) else {
            return
        }
        self.execute(request: request) { (data, error) in
            //print("result:", String(bytes: data!, encoding: .utf8)!)
            var retData: (String, String, String)?
            var retError: UserAccountAPIClientError?
            
            if error != nil {
                retError = .httpError
            } else if let data = data {
                do {
                    if let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] {
                        guard let email = dictionary["email"] as? String,
                              let password = dictionary["password"] as? String,
                              let avatarUrl = dictionary["avatar_url"] as? String else {
                                completionHandler(nil, .invalidDataError)
                                return
                        }
                        retData = (email, password, avatarUrl)
                    }
                } catch {
                    retError = .dataSerializationError
                }
            }
            completionHandler(retData, retError)
        }
    }
    
    func createAvatar(of userId: Int, with image: Data, token: String, completionHandler: @escaping (_ avatarUrl: String?, _ error: UserAccountAPIClientError?) -> Void) {
        
        let headerParams = ["Content-Type" : "application/json", "Authorization" : "Token \(token)"]
        let bodyParams = ["avatar": image.base64EncodedString()]
        let method = UserAccountAPIClientMethod.getAndPostAvatar.rawValue.replacingOccurrences(of: ":userid", with: String(userId))
        guard let request = self.createURLRequest(from: method, httpMethod: .post, headerParams: headerParams, bodyParams: bodyParams) else {
            return
        }
        self.execute(request: request) { (data, error) in
            //print("result:", String(bytes: data!, encoding: .utf8)!)
            var retData: String?
            var retError: UserAccountAPIClientError?
            
            if error != nil {
                retError = .httpError
            } else if let data = data {
                do {
                    if let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] {
                        guard let avatarUrl = dictionary["avatar_url"] as? String else {
                                completionHandler(nil, .invalidDataError)
                                return
                        }
                        retData = avatarUrl
                    }
                } catch {
                    retError = .dataSerializationError
                }
            }
            completionHandler(retData, retError)
        }
    }
    
    func getUserAvatar(from avatarUrl: String, token: String, completionHandler: @escaping (_ avatar: Data?, _ error: UserAccountAPIClientError?) -> Void) {
        let headerParams = ["Authorization" : "Token \(token)"]
        guard let request = self.createURLRequest(from: avatarUrl, httpMethod: .get, headerParams: headerParams) else {
            return
        }
        self.execute(request: request) { (data, error) in
            var retData: Data?
            var retError: UserAccountAPIClientError?
            
            if error != nil {
                retError = .httpError
            } else if let data = data {
                do {
                    if let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] {
                        guard let strImage = dictionary["avatar"] as? String, let imageData = Data.init(base64Encoded: strImage) else {
                            completionHandler(nil, .invalidDataError)
                            return
                        }
                        retData = imageData
                    }
                } catch {
                    retError = .dataSerializationError
                }
            }
            completionHandler(retData, retError)
        }
    }
    
    // MARK: PRIVATE METHODS
    private func createURLRequest(from urlString:String, httpMethod: HttpMethod, queryParams:[String: String] = [:], headerParams:[String: String] = [:], bodyParams:[String: String] = [:]) -> URLRequest? {
        // create url
        var urlComponents = URLComponents(string: urlString)
        
        // append query params
        var queryItems = [URLQueryItem]()
        for (key, value) in queryParams {
            let query = URLQueryItem(name: key, value: value)
            queryItems.append(query)
        }
        urlComponents?.queryItems = queryItems
        guard let url = urlComponents?.url else { return nil }
        print("WAM URL:\(url.absoluteString)")
        
        // create request object
        var request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 15)
        request.httpMethod = httpMethod.rawValue
        
        // append header params
        headerParams.forEach { request.addValue($1, forHTTPHeaderField: $0) }
        
        // append json body
        let jsonData = try? JSONSerialization.data(withJSONObject: bodyParams, options: .prettyPrinted)
        request.httpBody = jsonData
        
        return request
    }
    
    func execute(request: URLRequest, completionHandler: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        let dataTask = urlSession.dataTask(with: request) { (data, urlResponse, error) in
            DispatchQueue.main.async {
                completionHandler(data, error)
            }
        }
        dataTask.resume()
    }
}
