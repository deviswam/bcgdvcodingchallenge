//
//  LoginViewController.swift
//  BCGDigitalVentureTest
//
//  Created by Waheed Malik on 10/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    var presenter: LoginPresenter?
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        errorLabel.text = ""
        loginButton.isEnabled = false
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        presenter?.submit()
    }
    
    @IBAction func emailTextFieldEditingChanged(_ sender: UITextField) {
        if let text = sender.text {
            presenter?.email = text
        }
    }
    
    @IBAction func passwordTextFieldEditingChanged(_ sender: UITextField) {
        if let text = sender.text {
            presenter?.password = text
        }
    }
}

extension LoginViewController: LoginPresenterViewDelegate {
    func canSubmitStatusDidChange(presenter: LoginPresenter, status: Bool) {
        loginButton.isEnabled = status
    }
    
    func errorMessageDidChange(presenter: LoginPresenter, message: String) {
        errorLabel.text = message
    }
}
