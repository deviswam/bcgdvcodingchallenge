//
//  ProfileInteractor.swift
//  BCGDVCodingChallenge
//
//  Created by Waheed Malik on 11/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

protocol ProfileInteractor {
    func postUserAvatar(with image: UIImage, completionHandler: @escaping (_ result: Result<Bool>) -> Void)
    func getUserAccountDetail(completionHandler: @escaping (_ result: Result<UserAccount>) -> Void)
}

class BCGProfileInteractor: ProfileInteractor {
    private let userAccountManager: UserAccountManager
    
    init(userAccountManager: UserAccountManager) {
        self.userAccountManager = userAccountManager
    }
    
    func postUserAvatar(with image: UIImage, completionHandler: @escaping (_ result: Result<Bool>) -> Void) {
        self.userAccountManager.saveUserAvatar(with: image) { (result: Result<Bool>) in
            completionHandler(result)
        }
    }
    
    func getUserAccountDetail(completionHandler: @escaping (_ result: Result<UserAccount>) -> Void) {
        self.userAccountManager.getUserAccountDetail { (result: Result<UserAccount>) in
            completionHandler(result)
        }
    }
}
