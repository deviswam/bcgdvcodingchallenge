//
//  UserAccountAPIClientTests.swift
//  BCGDVCodingChallenge
//
//  Created by Waheed Malik on 10/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import BCGDVCodingChallenge

extension UserAccountAPIClientTests {
    class MockURLSession: URLSession {
        typealias Handler = (Data?, URLResponse?, Error?) -> Void
        
        var dataTaskRequestMethodCalled = false
        var urlRequest : URLRequest?
        var completionHandler : Handler?
        var dataTask = MockURLSessionDataTask()
        
        override func dataTask(with urlRequest: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
            dataTaskRequestMethodCalled = true
            self.urlRequest = urlRequest
            self.completionHandler = completionHandler
            return dataTask
        }
    }
    
    class MockURLSessionDataTask : URLSessionDataTask {
        var resumeGotCalled = false
        override func resume() {
            resumeGotCalled = true
        }
    }
}

class UserAccountAPIClientTests: XCTestCase {
    var sut: BCGUserAccountAPIClient!
    
    override func setUp() {
        super.setUp()
        sut = BCGUserAccountAPIClient()
        UserAccountAPIClientStubs.loadStubs()
    }
    
    func testConformanceToUserAccountAPIClient() {
        XCTAssertTrue((sut as AnyObject) is UserAccountAPIClient)
    }
    
    func testCreateSession_shouldAskURLSessionToCreateSession() {
        // Arrange
        let mockURLSession = MockURLSession()
        sut = BCGUserAccountAPIClient(urlSession: mockURLSession)
        
        //Act
        sut.createSession(with: "abc@gmail.com", password: "password") { (userid, token) in
            
        }
        
        // Assert
        XCTAssert(mockURLSession.dataTaskRequestMethodCalled)
    }
    
    func testCreateSession_whenNewSessionCreated_ReturnsUserIdAndTokenWithNilError() {
        // Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        sut = BCGUserAccountAPIClient()
        var receivedData: (userId: Int, token: String)?
        var receivedError: Error?
        
        //Act
        sut.createSession(with: "", password: "") { (data, error) in
            receivedData = data
            receivedError = error
            asynExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 1.0) { error in
            // Assert
            XCTAssertEqual(Int(stub_userId)!, receivedData?.userId)
            XCTAssertEqual(stub_token, receivedData?.token)
            XCTAssertNil(receivedError)
        }
    }
    
//    func testGetDetailsOfUser_whenUserDetailsFound_ReturnsUserEmailAndPasswordAndAvatarUrlWithNilError() {
//        // Arrange
//        let asynExpectation = expectation(description: "AsynFunction")
//        sut = BCGUserAccountAPIClient()
//        var receivedData: (email: String, password: String, avatarUrl: String)?
//        var receivedError: Error?
//        
//        //Act
//        sut.getDetails(of: 123) { (data, error) in
//            receivedData = data
//            receivedError = error
//            asynExpectation.fulfill()
//        }
//        
//        waitForExpectations(timeout: 1.0) { error in
//            // Assert
//            XCTAssertEqual(stub_email, receivedData?.email)
//            XCTAssertEqual(stub_avatarUrl, receivedData?.avatarUrl)
//            XCTAssertNil(receivedError)
//        }
//    }
    
    func testCreateAvatar_whenAvatarCreated_ReturnsAvatarUrlWithNilError() {
        // Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        sut = BCGUserAccountAPIClient()
        var receivedAvatarUrl: String?
        var receivedError: Error?
        
        //Act
        sut.createAvatar(of: 123, with: Data()) { (avatarUrl, error) in
            receivedAvatarUrl = avatarUrl
            receivedError = error
            asynExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 1.0) { error in
            // Assert
            XCTAssertEqual(stub_avatarUrl, receivedAvatarUrl!)
            XCTAssertNil(receivedError)
        }
    }
    
    override func tearDown() {
        super.tearDown()
        UserAccountAPIClientStubs.unloadStubs()
    }
}
