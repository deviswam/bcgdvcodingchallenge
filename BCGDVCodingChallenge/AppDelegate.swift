//
//  AppDelegate.swift
//  BCGDVCodingChallenge
//
//  Created by Waheed Malik on 10/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appRouter: AppRouter!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow()
        appRouter = AppRouter(window: window!)
        appRouter.start()
        window?.makeKeyAndVisible()
        
        print("Bundle path: ", Bundle.main.bundlePath)
        
        return true
    }
}

