//
//  LoginRouter.swift
//  BCGDVCodingChallenge
//
//  Created by Waheed Malik on 11/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

protocol LoginRouterDelegate: class {
    func loginRouterDidFinish(loginRouter: LoginRouter)
}

class LoginRouter: Router {
    weak var delegate: LoginRouterDelegate?
    let window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as? LoginViewController {
            let loginInteractor = BCGLoginInteractor(userAccountManager: SharedComponentsDir.userAccountManager)
            let loginPresenter = BCGLoginPresenter(interactor: loginInteractor, routerDelegate: self, viewDelegate: loginVC)
            loginVC.presenter = loginPresenter
            window.rootViewController = loginVC
        }
    }
}

extension LoginRouter: LoginPresenterRouterDelegate {
    func userDidLogIn(presenter: LoginPresenter) {
        delegate?.loginRouterDidFinish(loginRouter: self)
    }
}
