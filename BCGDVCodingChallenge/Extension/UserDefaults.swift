//
//  UserDefaults.swift
//  BCGDVCodingChallenge
//
//  Created by Waheed Malik on 12/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

extension UserDefaults {
    func save(email: String) {
        self.save(value: email, forKey: "email")
    }
    
    func getEmail() -> String? {
        return self.getValue(forKey: "email") as? String
    }
    
    func save(password: String) {
        self.save(value: password, forKey: "password")
    }
    
    func getPassword() -> String? {
        return self.getValue(forKey: "password") as? String
    }
    
    func save(userId: Int) {
        self.save(value: userId, forKey: "userId")
    }
    
    func getUserId() -> Int? {
        return self.getValue(forKey: "userId") as? Int
    }
    
    func save(accessToken: String) {
        self.save(value: accessToken, forKey: "accessToken")
    }
    
    func getAccessToken() -> String? {
        return self.getValue(forKey: "accessToken") as? String
    }
    
    func save(value: Any, forKey key: String) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
        defaults.synchronize()
    }
    
    func getValue(forKey key: String) -> Any? {
        let defaults = UserDefaults.standard
        return defaults.value(forKey: key)
    }
}
