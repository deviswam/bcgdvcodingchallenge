//
//  LoginPresenterTests.swift
//  BCGDVCodingChallenge
//
//  Created by Waheed Malik on 11/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import BCGDVCodingChallenge

extension LoginPresenterTests {
    class MockLoginPresenterViewDelegate: LoginPresenterViewDelegate {
        func canSubmitStatusDidChange(presenter: LoginPresenter, status: Bool) {
            
        }
        
        var errorMessageReceived: String?
        func errorMessageDidChange(presenter: LoginPresenter, message: String) {
            errorMessageReceived = message
        }
    }
    
    class MockLoginPresenterRouterDelegate: LoginPresenterRouterDelegate {
        var userDidLogInCalled = false
        func userDidLogIn(presenter: LoginPresenter) {
            userDidLogInCalled = true
        }
    }
    
    class MockLoginInteractor: LoginInteractor {
        var email: String?
        var password: String?
        var completionHandler: ((Result<Bool>) -> Void)?
        func authenticate(with email: String, password: String, completionHandler: @escaping (_ result: Result<Bool>) -> Void) {
            self.email = email
            self.password = password
            self.completionHandler = completionHandler
        }
    }
}

class LoginPresenterTests: XCTestCase {
    
    var mockViewDelegate: MockLoginPresenterViewDelegate!
    var mockRouterDelegate: MockLoginPresenterRouterDelegate!
    var mockInteractor: MockLoginInteractor!
    var sut: BCGLoginPresenter!
    
    override func setUp() {
        super.setUp()
        mockViewDelegate = MockLoginPresenterViewDelegate()
        mockRouterDelegate = MockLoginPresenterRouterDelegate()
        mockInteractor = MockLoginInteractor()
        sut = BCGLoginPresenter(interactor: mockInteractor, routerDelegate: mockRouterDelegate, viewDelegate: mockViewDelegate)
    }
    
    func testConformanceToLoginPresenterProtocol() {
        XCTAssertTrue((sut as AnyObject) is LoginPresenter)
    }
    
    func testSubmit_asksLoginInteractorToAuthenticateWithEmailAndPassword() {
        // Arrange
        sut.email = "abc@gmail.com"
        sut.password = "password"
        
        // Act
        sut.submit()
        
        // Assert
        XCTAssertEqual(sut.email, mockInteractor.email!)
        XCTAssertEqual(sut.password, mockInteractor.password!)
    }
    
    func testSubmit_whenAuthenticatedSuccessfully_shouldInformRouterDelegateAboutSuccessfulLogin() {
        // Arrange
        sut.email = "abc@gmail.com"
        sut.password = "password"
        
        // Act
        sut.submit()
        mockInteractor.completionHandler?(.success(true))
        
        // Assert
        XCTAssertTrue(mockRouterDelegate.userDidLogInCalled)
    }
    
    func testSubmit_whenAuthenticationFails_shouldInformViewDelegateWithErrorMesage() {
        // Arrange
        sut.email = "abc@gmail.com"
        sut.password = "password"
        
        // Act
        sut.submit()
        mockInteractor.completionHandler?(.failure(UserAccountManagerError.authenticationFailed))
        
        // Assert
        XCTAssertEqual(mockViewDelegate.errorMessageReceived!, UserAccountManagerError.authenticationFailed.localizedDescription)
    }
    
}
