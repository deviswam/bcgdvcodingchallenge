//
//  UserAccountAPIClientStubs.swift
//  BCGDVCodingChallenge
//
//  Created by Waheed Malik on 10/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation
import OHHTTPStubs

enum StubError: Int, Error {
    case dataNotFound = 404
    case authorizationFailed = 401
}

let stub_userId = "123"
let stub_token = "ab3cd9j4ks73hf7g"
let stub_avatarUrl = "/users/123/avatar"

class UserAccountAPIClientStubs {
    
    static func loadStubs() {
        // Stub: Authenticate User
        stub(condition: isPath("/sessions/new") && isMethodPOST()) { request in
            // store received email and password on the server.
            if let email = request.emailInHttpBody, let password = request.passwordInHttpBody {
                UserDefaults.standard.save(email: email)
                UserDefaults.standard.save(password: password)
            }
         
            // send response
            let obj = ["userid": stub_userId, "token": stub_token]
            return OHHTTPStubsResponse(jsonObject: obj, statusCode: 200, headers: nil)
        }
        
        // Stub: Get User Details
        stub(condition: isPath("/users/123") && isMethodGET()) { request in
            // check for authorization token
            guard request.value(forHTTPHeaderField: "Authorization") != nil else {
                return OHHTTPStubsResponse(error: StubError.authorizationFailed)
            }
            
            // get user email and password from server and send it as a response.
            guard let email = UserDefaults.standard.getEmail(), let password = UserDefaults.standard.getPassword() else {
                return OHHTTPStubsResponse(error: StubError.dataNotFound)
            }
            
            // send response
            let obj = ["email": email, "password": password, "avatar_url": stub_avatarUrl]
            return OHHTTPStubsResponse(jsonObject: obj, statusCode: 200, headers: nil)
        }
        
        // Stub: Create Avatar
        stub(condition: isPath(stub_avatarUrl) && isMethodPOST()) { request in
            // check for authorization token
            guard request.value(forHTTPHeaderField: "Authorization") != nil else {
                return OHHTTPStubsResponse(error: StubError.authorizationFailed)
            }
            
            // store received avatar image on the server.
            if let avatarImageEncodedString = request.avatarImageInHttpBody {
                UserDefaults.standard.save(value: avatarImageEncodedString, forKey: stub_avatarUrl)
            }
            
            let obj = ["avatar_url": stub_avatarUrl]
            return OHHTTPStubsResponse(jsonObject: obj, statusCode: 200, headers: nil)
        }
        
        // Stub: Get Avatar
        stub(condition: isPath(stub_avatarUrl) && isMethodGET()) { request in
            // check for authorization token
            guard request.value(forHTTPHeaderField: "Authorization") != nil else {
                return OHHTTPStubsResponse(error: StubError.authorizationFailed)
            }
            
            // use avatar_url to load avatar from server.
            guard let avatarEncodedImage = UserDefaults.standard.getValue(forKey: stub_avatarUrl) else {
                return OHHTTPStubsResponse(error: StubError.dataNotFound)
            }
            
            let obj = ["avatar": avatarEncodedImage]
            return OHHTTPStubsResponse(jsonObject: obj, statusCode: 200, headers: nil)
        }
        
        OHHTTPStubs.setEnabled(true)
    }
    
    static func unloadStubs() {
        OHHTTPStubs.setEnabled(false)
    }
}
