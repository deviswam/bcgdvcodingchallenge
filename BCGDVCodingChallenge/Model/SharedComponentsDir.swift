//
//  SharedComponentsDir.swift
//  BCGDVCodingChallenge
//
//  Created by Waheed Malik on 11/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

class SharedComponentsDir {
    static let userAccountManager: UserAccountManager = {
        let userAccountManager = BCGUserAccountManager(userAccountApiClient: SharedComponentsDir.userAccountAPIClient)
        return userAccountManager
    }()
    
    static let userAccountAPIClient: UserAccountAPIClient = {
        let apiClient = BCGUserAccountAPIClient()
        return apiClient
    }()
}
