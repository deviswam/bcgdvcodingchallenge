//
//  ProfileViewController.swift
//  BCGDigitalVentureTest
//
//  Created by Waheed Malik on 10/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var errorLabel: UILabel!
    
    var presenter: ProfilePresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        errorLabel.text = ""
        
        //ask for user details.
        presenter?.getUserDetails()
    }
    
    @IBAction func avatarTapGesture(_ sender: UITapGestureRecognizer) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        self.present(imagePickerController, animated: true, completion: nil)
    }
}

extension ProfileViewController: ProfilePresenterViewDelegate {
    func userDetailsLoaded(presenter: ProfilePresenter, email: String, password: String) {
        emailLabel.text = email
        passwordLabel.text = password
    }
    
    func avatarImageLoaded(presenter: ProfilePresenter, avatarImage: UIImage) {
        avatarImageView.image = avatarImage
    }
    
    func errorMessageDidChange(presenter: ProfilePresenter, message: String) {
        errorLabel.text = message
    }
}

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            //print(image)
            presenter?.postUserAvatar(image: image)
        } else {
            print("something went wrong")
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
