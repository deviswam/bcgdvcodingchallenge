//
//  Router.swift
//  BCGDVCodingChallenge
//
//  Created by Waheed Malik on 11/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol Router: class {
    func start()
}
