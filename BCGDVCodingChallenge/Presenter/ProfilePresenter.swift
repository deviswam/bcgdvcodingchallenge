//
//  ProfilePresenter.swift
//  BCGDVCodingChallenge
//
//  Created by Waheed Malik on 11/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

protocol ProfilePresenterViewDelegate {
    func userDetailsLoaded(presenter: ProfilePresenter, email: String, password: String)
    func avatarImageLoaded(presenter: ProfilePresenter, avatarImage: UIImage)
    func errorMessageDidChange(presenter: ProfilePresenter, message: String)
}

protocol ProfilePresenter {
    func getUserDetails()
    func postUserAvatar(image: UIImage)
}

class BCGProfilePresenter: ProfilePresenter {
    private let interactor: ProfileInteractor
    private let viewDelegate: ProfilePresenterViewDelegate
    init(interactor: ProfileInteractor, viewDelegate: ProfilePresenterViewDelegate) {
        self.interactor = interactor
        self.viewDelegate = viewDelegate
    }
    
    func getUserDetails() {
        self.interactor.getUserAccountDetail { (result: Result<UserAccount>) in
            switch result {
            case .success(let userAccount):
                self.errorMessage = ""
                if let email = userAccount.email, let password = userAccount.password {
                    self.viewDelegate.userDetailsLoaded(presenter: self, email: "Email: " + email, password: "Password: " + password)
                }
                if let avatar = userAccount.avatar {
                    self.viewDelegate.avatarImageLoaded(presenter: self, avatarImage: avatar)
                }
            case .failure(let error):
                self.errorMessage = error.localizedDescription
            }
        }
    }
    
    func postUserAvatar(image: UIImage) {
        self.interactor.postUserAvatar(with: image) { (result: Result<Bool>) in
            switch result {
            case .success(let isAvatarPosted):
                self.errorMessage = ""
                if isAvatarPosted {
                    self.viewDelegate.avatarImageLoaded(presenter: self, avatarImage: image)
                }
            case .failure(let error):
                self.errorMessage = error.localizedDescription
            }
        }
    }
    
    private(set) var errorMessage: String = "" {
        didSet {
            if oldValue != errorMessage {
                viewDelegate.errorMessageDidChange(presenter: self, message: errorMessage)
            }
        }
    }
}
