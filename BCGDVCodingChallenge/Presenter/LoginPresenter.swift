//
//  LoginPresenter.swift
//  BCGDVCodingChallenge
//
//  Created by Waheed Malik on 11/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol LoginPresenterViewDelegate: class {
    func canSubmitStatusDidChange(presenter: LoginPresenter, status: Bool)
    func errorMessageDidChange(presenter: LoginPresenter, message: String)
}

protocol LoginPresenterRouterDelegate: class {
    func userDidLogIn(presenter: LoginPresenter)
}

protocol LoginPresenter {
    // Email and Password
    var email: String { get set }
    var password: String { get set }
    
    // Submit
    func submit()
    
    // Errors
    var errorMessage: String { get }
}

class BCGLoginPresenter: LoginPresenter {
    private let interactor: LoginInteractor
    private weak var routerDelegate: LoginPresenterRouterDelegate!
    private weak var viewDelegate: LoginPresenterViewDelegate!
    
    private var emailIsValidFormat: Bool = false
    private var passwordIsValidFormat: Bool = false
    
    init(interactor: LoginInteractor, routerDelegate: LoginPresenterRouterDelegate, viewDelegate: LoginPresenterViewDelegate) {
        self.interactor = interactor
        self.routerDelegate = routerDelegate
        self.viewDelegate = viewDelegate
    }
    
    // Email
    var email: String = "" {
        didSet {
            if oldValue != email {
                let oldCanSubmit = canSubmit
                emailIsValidFormat = validateEmailFormat(email)
                if canSubmit != oldCanSubmit {
                    viewDelegate.canSubmitStatusDidChange(presenter: self, status: canSubmit)
                }
            }
        }
    }
    
    // Password
    var password: String = "" {
        didSet {
            if oldValue != password {
                let oldCanSubmit = canSubmit
                passwordIsValidFormat = validatePasswordFormat(password)
                if canSubmit != oldCanSubmit {
                    viewDelegate.canSubmitStatusDidChange(presenter: self, status: canSubmit)
                }
            }
        }
    }
    
    // Submit
    private var canSubmit: Bool {
        return emailIsValidFormat && passwordIsValidFormat
    }
    
    
    // Errors
    private(set) var errorMessage: String = "" {
        didSet {
            if oldValue != errorMessage {
                viewDelegate.errorMessageDidChange(presenter: self, message: errorMessage)
            }
        }
    }
    
    private func validateEmailFormat(_ email: String) -> Bool {
        let REGEX: String
        REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,32}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: email)
    }
    
    
    // Validate password is at least 6 characters
    private func validatePasswordFormat(_ password: String) -> Bool {
        let trimmedString = password.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return trimmedString.characters.count > 5
    }
    
    
    func submit() {
        errorMessage = ""
        guard canSubmit else {
            errorMessage = NSLocalizedString("NOT_READY_TO_SUBMIT", comment: "")
            return
        }
        
        interactor.authenticate(with: email, password: password) { (result: Result<Bool>) in
            switch result {
                case .success(let isAuthenticated):
                    self.errorMessage = ""
                    if isAuthenticated {
                        self.routerDelegate.userDidLogIn(presenter: self)
                    }
                case .failure(let error):
                    self.errorMessage = error.localizedDescription
            }
        }
    }
}
