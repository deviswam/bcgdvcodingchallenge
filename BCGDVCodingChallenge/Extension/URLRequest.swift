//
//  URLRequest.swift
//  BCGDVCodingChallenge
//
//  Created by Waheed Malik on 12/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

extension URLRequest {
    var emailInHttpBody: String? {
        guard let email = self.getJSONParameterValueFromHttpBody(forKey: "email") as? String else { return nil }
        return email
    }
    
    var passwordInHttpBody: String? {
        guard let password = self.getJSONParameterValueFromHttpBody(forKey: "password") as? String else { return nil }
        return password
    }
    
    var avatarImageInHttpBody: String? {
        guard let imageStr = self.getJSONParameterValueFromHttpBody(forKey: "avatar") as? String else { return nil }
        return imageStr
    }
    
    func getJSONParameterValueFromHttpBody(forKey key: String) -> Any? {
        let httpBody = (self as NSURLRequest).ohhttpStubs_HTTPBody()
        guard let httpBodyData = httpBody else { return nil }
        do {
            if let dictionary = try JSONSerialization.jsonObject(with: httpBodyData, options: []) as? [String: Any] {
                return dictionary[key]
            }
        } catch(let error) {
            print(error.localizedDescription)
        }
        return nil
    }
}
