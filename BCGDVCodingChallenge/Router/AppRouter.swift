//
//  AppRouter.swift
//  BCGDVCodingChallenge
//
//  Created by Waheed Malik on 11/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class AppRouter : Router {
    fileprivate let LOGIN_KEY: String  = "Login"
    fileprivate let PROFILE_KEY: String  = "Profile"
    
    var window: UIWindow
    var routers = [String:Router]()
    
    init(window: UIWindow)
    {
        self.window = window
    }
    
    func start()
    {
        UserAccountAPIClientStubs.loadStubs()
        if isLoggedIn {
            showProfile()
        } else {
            showLogin()
        }
    }
}

extension AppRouter: LoginRouterDelegate
{
    var isLoggedIn: Bool {
        return SharedComponentsDir.userAccountManager.isUserLoggedIn();
    }
    
    func showLogin()
    {
        let loginRouter = LoginRouter(window: window)
        routers[LOGIN_KEY] = loginRouter
        loginRouter.delegate = self
        loginRouter.start()
    }
    
    func loginRouterDidFinish(loginRouter: LoginRouter)
    {
        routers[LOGIN_KEY] = nil
        showProfile()
    }
}


extension AppRouter: ProfileRouterDelegate
{
    func showProfile()
    {
        let profileRouter = ProfileRouter(window: window)
        routers[PROFILE_KEY] = profileRouter
        profileRouter.delegate = self
        profileRouter.start()
    }
    
    func profileRouterDidFinish(profileRouter: ProfileRouter)
    {
         routers[PROFILE_KEY] = nil
    }
}
