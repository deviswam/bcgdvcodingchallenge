//
//  UserAccountManager.swift
//  BCGDVCodingChallenge
//
//  Created by Waheed Malik on 11/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

enum UserAccountManagerError: LocalizedError {
    case authenticationFailed
    case userDetailsFetchFailed
    case avatarCreationFailed
    case inValidUser
    case inValidAvatarImageData
    case avatarImageNotFound
    case avatarImageTooBig
    case connectionError
    
    var errorDescription: String? {
        switch self {
        case .authenticationFailed:
            return "Unable to authenticate user"
        case .userDetailsFetchFailed:
            return "Unable to fetch user details"
        case .avatarCreationFailed:
            return "Unable to create avatar"
        case .inValidUser:
            return "Invalid User"
        case .inValidAvatarImageData:
            return "Avatar Image data is invalid"
        case .avatarImageNotFound:
            return "Avatar image Not Found"
        case .avatarImageTooBig:
            return "Avatar image can't be greater than 1mb"
        case .connectionError:
            return "Connection Error"
        }
    }
}

enum UserAccountManagerMethod {
    case authenticateUser
    case fetchUserDetails
    case createAvatar
}

protocol UserAccountManager {
    func isUserLoggedIn() -> Bool
    func authenticate(with email: String, password: String, completionHandler: @escaping (_ result: Result<Bool>) -> Void)
    func getUserAccountDetail(completionHandler: @escaping (Result<UserAccount>) -> Void)
    func saveUserAvatar(with image: UIImage, completionHandler: @escaping (_ result: Result<Bool>) -> Void)
}

class BCGUserAccountManager: UserAccountManager {

    private let userAccountApiClient: UserAccountAPIClient
    private var userAccount: UserAccount?
    
    init(userAccountApiClient: UserAccountAPIClient) {
        self.userAccountApiClient = userAccountApiClient
        self.userAccount = self.loadUserAccount()
    }
    
    func isUserLoggedIn() -> Bool {
        return UserDefaults.standard.getAccessToken() != nil
    }
    
    func authenticate(with email: String, password: String, completionHandler: @escaping (_ result: Result<Bool>) -> Void) {
        self.userAccountApiClient.createSession(with: email, password: password) { (data: (userId: Int, token: String)?, error: UserAccountAPIClientError?) in
            if let data = data, error == nil {
                self.userAccount = self.createUserAccount(userId: data.userId, token: data.token, email: email, password: password)
                self.saveUserInfo(userId: data.userId, token: data.token)
                completionHandler(.success(true))
            } else if let error = error {
                let managerError = self.userAccountManagerError(from: error, method: .authenticateUser)
                completionHandler(.failure(managerError))
            }
        }
    }
    
    func getUserAccountDetail(completionHandler: @escaping (Result<UserAccount>) -> Void) {
        guard var userAccount = self.userAccount, let token = userAccount.token else {
            completionHandler(.failure(UserAccountManagerError.inValidUser))
            return
        }
        self.userAccountApiClient.getDetails(of: userAccount.userId, token: token) { (data: (email: String, password: String, avatarUrl: String)?, error: UserAccountAPIClientError?) in
            if let data = data, error == nil {
                userAccount.avatar_url = data.avatarUrl
                userAccount.email = data.email
                userAccount.password = data.password
                
                self.userAccountApiClient.getUserAvatar(from: userAccount.avatar_url!, token: token, completionHandler: { (imageData, error) in
                    if let imageData = imageData, error == nil, let image = UIImage(data: imageData) {
                        userAccount.avatar = image
                    }
                    completionHandler(.success(userAccount))
                })
                
            } else if let error = error {
                let managerError = self.userAccountManagerError(from: error, method: .fetchUserDetails)
                completionHandler(.failure(managerError))
            }
        }
    }
    
    
    
    func saveUserAvatar(with image: UIImage, completionHandler: @escaping (_ result: Result<Bool>) -> Void) {
        guard var userAccount = self.userAccount, let token = userAccount.token else {
            completionHandler(.failure(UserAccountManagerError.inValidUser))
            return
        }
        guard let imageData = UIImagePNGRepresentation(image) else {
            completionHandler(.failure(UserAccountManagerError.inValidAvatarImageData))
            return
        }
        // check for 1mb image size
        guard (imageData as NSData).length <= (1024 * 1024) else {
            completionHandler(.failure(UserAccountManagerError.avatarImageTooBig))
            return
        }
        //print("WAM: ImageSize: ", (imageData as NSData).length)
        self.userAccountApiClient.createAvatar(of: userAccount.userId, with: imageData, token: token) { (avatarUrl: String?, error: UserAccountAPIClientError?) in
            if let avatarUrl = avatarUrl, error == nil {
                userAccount.avatar_url = avatarUrl
                completionHandler(.success(true))
            } else if let error = error {
                let managerError = self.userAccountManagerError(from: error, method: .createAvatar)
                completionHandler(.failure(managerError))
            }
        }
    }
    
    private func saveUserInfo(userId: Int, token: String) {
        let defaults = UserDefaults.standard
        defaults.save(userId: userId)
        defaults.save(accessToken: token)
    }
    
    private func loadUserAccount() -> UserAccount? {
        if userAccount != nil {
            return userAccount
        } else {
            guard let userId = UserDefaults.standard.getUserId(), let token = UserDefaults.standard.getAccessToken() else { return nil }
            return self.createUserAccount(userId: userId, token: token)
        }
    }
    
    private func createUserAccount(userId: Int, token: String? = nil, email: String? = nil, password: String? = nil) -> UserAccount {
        let userAccount = BCGUserAccount(userId: userId)
        userAccount.token = token
        userAccount.email = email
        userAccount.password = password
        
        return userAccount
    }
    
    private func userAccountManagerError(from apiClientError: UserAccountAPIClientError, method: UserAccountManagerMethod) -> UserAccountManagerError {
        switch apiClientError {
        case .dataSerializationError, .invalidDataError:
            switch method {
            case .authenticateUser:
                return .authenticationFailed
            case .fetchUserDetails:
                return .userDetailsFetchFailed
            case .createAvatar:
                return .avatarCreationFailed
            }
        case .httpError:
            return .connectionError
        }
    }
}
