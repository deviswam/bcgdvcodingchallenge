//
//  UserAccount.swift
//  BCGDVCodingChallenge
//
//  Created by Waheed Malik on 11/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

protocol UserAccount {
    var userId: Int { get }
    var email: String? { get set }
    var password: String? { get set }
    var avatar_url: String? { get set }
    var token: String? { get set }
    var avatar: UIImage? { get set }
}

class BCGUserAccount: UserAccount {
    var userId: Int
    var email: String?
    var password: String?
    var avatar_url: String?
    var token: String?
    var avatar: UIImage?
    
    init(userId: Int) {
        self.userId = userId
    }
}

func == (lhs: UserAccount, rhs: UserAccount) -> Bool {
    if lhs.userId == rhs.userId {
        return true
    }
    return false
}
