//
//  ProfileRouter.swift
//  BCGDVCodingChallenge
//
//  Created by Waheed Malik on 11/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

protocol ProfileRouterDelegate: class {
    func profileRouterDidFinish(profileRouter: ProfileRouter)
}

class ProfileRouter: Router {
    weak var delegate: ProfileRouterDelegate?
    let window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let profileVC = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as? ProfileViewController {
            let userAccountManager = SharedComponentsDir.userAccountManager
            let profileInteractor = BCGProfileInteractor(userAccountManager: userAccountManager)
            let profilePresenter = BCGProfilePresenter(interactor: profileInteractor, viewDelegate: profileVC)
            profileVC.presenter = profilePresenter
            window.rootViewController = profileVC
        }
    }
}
