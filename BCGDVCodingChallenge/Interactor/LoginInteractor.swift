//
//  LoginInteractor.swift
//  BCGDVCodingChallenge
//
//  Created by Waheed Malik on 11/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol LoginInteractor {
    func authenticate(with email: String, password: String, completionHandler: @escaping (_ result: Result<Bool>) -> Void)
}

class BCGLoginInteractor: LoginInteractor {
    private let userAccountManager: UserAccountManager
    
    init(userAccountManager: UserAccountManager) {
        self.userAccountManager = userAccountManager
    }
    
    func authenticate(with email: String, password: String, completionHandler: @escaping (_ result: Result<Bool>) -> Void) {
        self.userAccountManager.authenticate(with: email, password: password) { (result: Result<Bool>) in
            return completionHandler(result)
        }
    }
}
